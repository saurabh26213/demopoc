package com.hcl.currencyconvertpoc;

import org.junit.Test;

import static org.junit.Assert.*;

import com.hcl.currencyconvertpoc.currency.ui.MainActivity;

/**
 * CurrencyConvert local unit test, which will execute on the development machine (host).
 */
public class CurrencyConvertTest {
    MainActivity mainInstance = new MainActivity();

    @Test
    public void convertAEDtoAFN() {
        String result = mainInstance.exchangeCurrencyRates(ConstantTest.ETVALUE, ConstantTest.AED, ConstantTest.AFN);
        assertEquals("Convert Currency", "42.04", result);
    }
}