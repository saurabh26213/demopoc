package com.hcl.currencyconvertpoc.repository;

import androidx.lifecycle.MutableLiveData;
import androidx.test.core.app.ApplicationProvider;

import com.hcl.currencyconvertpoc.pojo.CurrencyList;
import com.hcl.currencyconvertpoc.viewmodel.CurrencyViewModel;

import org.junit.Before;
import org.junit.Test;

public class CurrencyReporsitoryTest {

    private CurrencyViewModel currencyViewModel = null;
    MutableLiveData<CurrencyList> mutableLiveData = null;

    @Before
    public void setUp() throws InterruptedException {
        currencyViewModel = new CurrencyViewModel(ApplicationProvider.getApplicationContext());
        mutableLiveData = (MutableLiveData<CurrencyList>) currencyViewModel.getCurrencyRates();
        Thread.sleep(5000);
    }

    @Test
    public void checkApiResponse() throws InterruptedException {
        assert (mutableLiveData != null);
    }

    @Test
    public void checkResponseList() throws InterruptedException {
        assert (mutableLiveData.getValue().rates != null);
    }
}