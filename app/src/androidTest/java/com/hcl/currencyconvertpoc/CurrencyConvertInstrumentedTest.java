package com.hcl.currencyconvertpoc;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.hcl.currencyconvertpoc.currency.ui.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Instrumented test, which will execute on an Android device.
 */
@RunWith(AndroidJUnit4.class)
public class CurrencyConvertInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);

    /**
     * Test to check if able to type on edit text box
     */
    @Test
    public void test_to_check_able_to_type_on_edittext() {
        // Context of the app under test.
        onView(withId(R.id.ammount)).perform(typeText("2"), closeSoftKeyboard());
    }

    /**
     * Test to check if able to select from drop down and button click
     */
    @Test
    public void test_to_check_widgets_UI_functioning() {
        onView(withId(R.id.ammount)).perform(typeText("2"), closeSoftKeyboard());
        onView(withId(R.id.spinner_currency_base)).perform(click());
        // Click on the first item from the list, which is a marker string: "Select base currency"
        onData(allOf(is(instanceOf(String.class)))).atPosition(0).perform(click());
        onView(withId(R.id.spinner_currency_exchange)).perform(click());
        onData(allOf(is(instanceOf(String.class)))).atPosition(1).perform(click());
        onView(withId(R.id.exchange)).perform(click());
    }
}