package com.hcl.currencyconvertpoc.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.hcl.currencyconvertpoc.pojo.CurrencyList;
import com.hcl.currencyconvertpoc.repository.CurrencyReporsitory;

public class CurrencyViewModel extends AndroidViewModel {
    private CurrencyReporsitory currencyRepository;

    public CurrencyViewModel(@NonNull Application application) {
        super(application);
        currencyRepository = new CurrencyReporsitory(application);
    }

    public LiveData<CurrencyList> getCurrencyRates() {
        return currencyRepository.getMutableLiveData();
    }
}
