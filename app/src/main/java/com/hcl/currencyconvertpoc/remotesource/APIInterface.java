package com.hcl.currencyconvertpoc.remotesource;

import com.hcl.currencyconvertpoc.pojo.CurrencyList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * interface to call API through retrofit.
 */
public interface APIInterface {

    @GET("api/latest")
    Call<CurrencyList> doGetCurrencyList(@Query("access_key") String access_key);


}
