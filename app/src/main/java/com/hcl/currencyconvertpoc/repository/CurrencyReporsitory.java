package com.hcl.currencyconvertpoc.repository;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.hcl.currencyconvertpoc.pojo.CurrencyList;
import com.hcl.currencyconvertpoc.pojo.Rates;
import com.hcl.currencyconvertpoc.remotesource.APIInterface;
import com.hcl.currencyconvertpoc.remotesource.RemoteAPI;
import com.hcl.currencyconvertpoc.util.Constants;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyReporsitory {
    private MutableLiveData<CurrencyList> mutableLiveData = new MutableLiveData<>();
    private Application application;

    public CurrencyReporsitory(Application application) {
        this.application = application;
    }
    /**
     * Remote API call and on success response send the data to UI thread.
     */
    public MutableLiveData<CurrencyList> getMutableLiveData() {
        Call<CurrencyList> call = RemoteAPI.getClient().create(APIInterface.class).doGetCurrencyList(Constants.API_KEY);
        call.enqueue(new Callback<CurrencyList>() {
            @Override
            public void onResponse(Call<CurrencyList> call, Response<CurrencyList> response) {
                CurrencyList mBlogWrapper = response.body();
                mutableLiveData.setValue(mBlogWrapper);
            }

            @Override
            public void onFailure(Call<CurrencyList> call, Throwable t) {
            }
        });
        return mutableLiveData;
    }
}

