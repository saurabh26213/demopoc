package com.hcl.currencyconvertpoc.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Pojo to parse the currency rates.
 */
public class Rates {

    @SerializedName("AED")
    public Double aED;
    @SerializedName("AFN")
    public Double aFN;
    @SerializedName("ALL")
    public Double aLL;
    @SerializedName("AMD")
    public Double aMD;
    @SerializedName("ANG")
    public Double aNG;
    @SerializedName("AOA")
    public Double aOA;
    @SerializedName("ARS")
    public Double aRS;
    @SerializedName("AUD")
    public Double aUD;
    @SerializedName("AWG")
    public Double aWG;
    @SerializedName("AZN")
    public Double aZN;
    @SerializedName("BAM")
    public Double bAM;
    @SerializedName("BBD")
    public Double bBD;
    @SerializedName("BDT")
    public Double bDT;
    @SerializedName("BGN")
    public Double bGN;
    @SerializedName("BHD")
    public Double bHD;
    @SerializedName("BIF")
    public Double bIF;
    @SerializedName("BMD")
    public Double bMD;
}