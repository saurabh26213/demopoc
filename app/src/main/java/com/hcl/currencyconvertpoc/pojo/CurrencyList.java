package com.hcl.currencyconvertpoc.pojo;

import com.google.gson.annotations.SerializedName;

public class CurrencyList {

    @SerializedName("base")
    public String base;
    @SerializedName("rates")
    public Rates rates;

}
