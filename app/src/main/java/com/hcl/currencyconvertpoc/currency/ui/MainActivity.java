package com.hcl.currencyconvertpoc.currency.ui;

import android.os.Bundle;

import com.hcl.currencyconvertpoc.R;
import com.hcl.currencyconvertpoc.pojo.CurrencyList;
import com.hcl.currencyconvertpoc.pojo.Rates;
import com.hcl.currencyconvertpoc.remotesource.APIInterface;
import com.hcl.currencyconvertpoc.remotesource.RemoteAPI;
import com.hcl.currencyconvertpoc.viewmodel.CurrencyViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.View;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    APIInterface apiInterface;
    int selectedBasePosition = 0;
    int selectedExcPosition = 0;
    String selectedbaseCurrency;
    String selectedexchCurrency;
    String etvalue;
    TextView tvexchangerates;
    ArrayList<Double> list;
    private CurrencyViewModel currencyViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        currencyViewModel = ViewModelProviders.of(this).get(CurrencyViewModel.class);
    }

    /**
     * Invoke viewmodel to get currency rate using retrofit call.
     */
    private void doCalc() {
        tvexchangerates = findViewById(R.id.tvexchangerates);
        tvexchangerates.setText("");
        currencyViewModel.getCurrencyRates().observe(this, new Observer<CurrencyList>() {
            @Override
            public void onChanged(CurrencyList rates) {
                CurrencyList currencyRates = rates;
                Rates rateList = currencyRates.rates;
                list = new ArrayList();
                list.add(rateList.aED);
                list.add(rateList.aFN);
                list.add(rateList.aLL);
                list.add(rateList.aMD);
                list.add(rateList.aNG);
                list.add(rateList.aOA);
                list.add(rateList.aRS);
                list.add(rateList.aUD);
                list.add(rateList.aWG);
                list.add(rateList.aZN);
                list.add(rateList.bAM);

                Double basevalue = list.get(selectedBasePosition);
                Double exchangevalue = list.get(selectedExcPosition);
                String resultExc = exchangeCurrencyRates(etvalue, basevalue, exchangevalue);
                tvexchangerates.setText(etvalue + " " + selectedbaseCurrency + " equals " + resultExc + " " + selectedexchCurrency);
            }
        });
    }

    public String exchangeCurrencyRates(String etvalue, Double basevalue, Double exchangevalue) {
        Double resultExchange = java.lang.Double.valueOf(etvalue) * (1 / basevalue * exchangevalue);
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(resultExchange);
    }

    /**
     * initialize the spinner and call selected listner of spinner
     */
    public void init() {

        final Spinner baseSpinner = findViewById(R.id.spinner_currency_base);
        final Spinner exchangeSpinner = findViewById(R.id.spinner_currency_exchange);
        apiInterface = RemoteAPI.getClient().create(APIInterface.class);
        baseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                selectedBasePosition = baseSpinner.getSelectedItemPosition();
                selectedbaseCurrency = baseSpinner.getSelectedItem().toString();
            }

            public void onNothingSelected(AdapterView<?> arg0) {

            }

        });
        exchangeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                selectedExcPosition = exchangeSpinner.getSelectedItemPosition();
                selectedexchCurrency = exchangeSpinner.getSelectedItem().toString();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }

        });
        Button clickButton = findViewById(R.id.exchange);
        clickButton.setOnClickListener((View v) -> {
            try {
                EditText et = findViewById(R.id.ammount);
                etvalue = et.getText().toString();
                if (etvalue.trim().length() > 0)
                    doCalc();
                else
                    Toast.makeText(getApplicationContext(), R.string.empty_value, Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}