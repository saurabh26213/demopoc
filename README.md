# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Currency Converter Demo POC ###

* Currency conversion Demo POC using live exchange rates
* Uses only 14-15 currency types for demo exchanging purpose.
* Use the API from http://fixer.io to retrieve the currency rates
* Version:1.0


### Fixer API key generation ###

* Fixer API Key is the unique key that is passed into the API base URL's access_key parameter in order to authenticate with the Fixer API.
* Append the API Key:
   http://data.fixer.io/api/latest? access_key = API_KEY

### Architecture ###
This app implements the MVVM architectural pattern using a single activity using a repository to fetch remote data.

### Built with ###
* LiveData : An observable data holder class.
* ViewModel : Class designed to store and manage UI-related data in a lifecycle conscious way.
* Retrofit: HTTP client to consume web API.
* Gson2.8: For parsing of JSON.


### Unit Test ###
* Run on host machine.
* Junit4: Testing Framework.
* Written unit test to test demo currency exchange rates.
* Written unit test to test API returning nonnull value.

### Instrumented Test ###
* Run on device.
* espresso3.2: Run instrumented test to test UI.

### Release apk ###
* Release apk is on release folder in project structure.

